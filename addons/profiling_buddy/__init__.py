import bpy
from . import modifier_profiling

bl_info = {
	"name": "Profiling Buddy",
	"author": "Simon Thommes",
	"version": (0,1),
	"blender": (3, 5, 0),
	"location": "Properties > Modifier > Profiling Buddy",
	"description": "Adds panels to profile execution times.",
	"category": "Workflow",
}

modules = [modifier_profiling]

def register():
	for m in modules:
		m.register()

def unregister():
	for m in modules:
		m.unregister()
