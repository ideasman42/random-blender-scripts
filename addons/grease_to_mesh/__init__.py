from . import grease_to_mesh


bl_info = {
	"name": "Grease to Mesh",
	"author": "Simon Thommes",
	"version": (0,2),
	"blender": (2, 93, 0),
	"location": "Sidebar > Misc",
	"description": "Tool to quickly convert a grease pencil animation into a mesh sequence.",
	"category": "Animation",
}

def register():
    grease_to_mesh.register()
    
def unregister():
    grease_to_mesh.unregister()