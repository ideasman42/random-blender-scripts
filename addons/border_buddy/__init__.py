from . import border_buddy


bl_info = {
	"name": "Border Buddy",
	"author": "Simon Thommes",
	"version": (0,1),
	"blender": (3, 0, 0),
	"location": "Properties > Output",
	"description": "Tool to store, manage and load render border selections.",
	"category": "Render",
}

def register():
    border_buddy.register()
    
def unregister():
    border_buddy.unregister()