import bpy

def border_update(self, context):
    bs = context.scene.border_saver_props
    active_border = bs.border_list[bs.border_index]
    
    if bs.prev == bs.border_index:
        return None
    else:
        context.scene.render.border_min_x = active_border.min_x
        context.scene.render.border_min_y = active_border.min_y
        context.scene.render.border_max_x = active_border.max_x
        context.scene.render.border_max_y = active_border.max_y
        bs.prev = bs.border_index
        return None

class BS_border_property(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name='Name', default='Border')
    min_x: bpy.props.FloatProperty(name='Min X', default=0.0)
    min_y: bpy.props.FloatProperty(name='Min Y', default=0.0)
    max_x: bpy.props.FloatProperty(name='Max X', default=1.0)
    max_y: bpy.props.FloatProperty(name='Max Y', default=1.0)

class BS_BorderSaverProps(bpy.types.PropertyGroup):
    border_list: bpy.props.CollectionProperty(type=BS_border_property)
    border_index: bpy.props.IntProperty(name="Index", default=0, update=border_update)
    prev: bpy.props.IntProperty(name="Index", default=0)

class BS_UL_border_list(bpy.types.UIList):
    def draw_item(
        self, context, layout, data, item, icon, active_data, active_propname, index
    ):
        layout.prop(item, "name", text='', emboss=False, icon="SELECT_SET")

class BS_border_add(bpy.types.Operator):
    """
    """
    bl_idname = "scene.border_add"
    bl_label = "Add Border"
    bl_options = {"REGISTER", "UNDO"}
    
    def execute(self, context):
        bs = context.scene.border_saver_props
        new_border = bs.border_list.add()
        
        new_border.min_x = context.scene.render.border_min_x
        new_border.min_y = context.scene.render.border_min_y
        new_border.max_x = context.scene.render.border_max_x
        new_border.max_y = context.scene.render.border_max_y
        
        bs.border_index = len(bs.border_list)-1
        
        return {'FINISHED'}

class BS_border_remove(bpy.types.Operator):
    """
    """
    bl_idname = "scene.border_remove"
    bl_label = "Remove Border"
    bl_options = {"REGISTER", "UNDO"}
    
    def execute(self, context):
        context.scene.border_saver_props.border_list.remove(context.scene.border_saver_props.border_index)
        return {'FINISHED'}

class BS_border_store(bpy.types.Operator):
    """
    """
    bl_idname = "scene.border_store"
    bl_label = "Store Border"
    bl_options = {"REGISTER", "UNDO"}
    
    def execute(self, context):
        bs = context.scene.border_saver_props
        active_border = bs.border_list[bs.border_index]
        
        active_border.min_x = context.scene.render.border_min_x
        active_border.min_y = context.scene.render.border_min_y
        active_border.max_x = context.scene.render.border_max_x
        active_border.max_y = context.scene.render.border_max_y
        
        return {'FINISHED'}


class BS_BorderSaverPanel(bpy.types.Panel):
    """
    """
    bl_label = "Border Buddy"
    bl_idname = "SCENE_PT_bordersaver"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "output"

    def draw(self, context):
        layout = self.layout
        row = layout.row()
        col = row.column()
        col.template_list(
            "BS_UL_border_list",
            "border_list",
            context.scene.border_saver_props,
            "border_list",
            context.scene.border_saver_props,
            "border_index",
            rows=3,
            )
        col = row.column(align=True)
        col.operator("scene.border_add", icon='ADD', text="")
        col.operator("scene.border_remove", icon='REMOVE', text="")
        col.operator("scene.border_store", icon='IMPORT', text="")

classes = [
            BS_border_property,
            BS_BorderSaverProps, 
            BS_UL_border_list,
            BS_border_add,
            BS_border_remove,
            BS_border_store,
            ]

def register():
    bpy.app.handlers.depsgraph_update_post.append(border_update)
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Scene.border_saver_props = bpy.props.PointerProperty(type=BS_BorderSaverProps)
    bpy.utils.register_class(BS_BorderSaverPanel)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    bpy.utils.unregister_class(BS_BorderSaverPanel)


if __name__ == "__main__":
    register()
