import bpy
import bmesh
from . import utils
from .containers import *
import numpy as np
from mathutils import Vector

import time

def straighten_uv_line(loops, uv_layer, interpolation_mode=None, auto_snap=False, preserve_length=False):
	''' Straightens uvs on a selected line and aligns them between endpoints
	'''
	# group loops by shared uv position/vertex and selection
	loop_groups = UVLoopCluster(loops, uv_layer).cleanup()

	endpoints = []
	# check selection topology and find endpoints
	for lg in loop_groups:
		neighbors = lg.neighbors.cleanup()
		if len(neighbors) not in [1,2]:
			print('Error: Selection does not fit expected line shape')
			return False
		elif len(neighbors)==1:
			endpoints.append(lg)
	if not len(endpoints) == 2:
		print('Error: Selection does not fit expected line shape. Incorrect number of endpoints.')
		return False

	# order loopgroups
	loop_groups = loop_groups.ordered(first=endpoints[0])
		
	uv_coords = [lg.uv for lg in loop_groups]
	start = uv_coords[0]
	end = uv_coords[-1]
	
	if preserve_length:
		length = 0.
		for p0, p1 in zip(uv_coords[:-1], uv_coords[1:]):
			length += (p0-p1).length
	
	if auto_snap:
		n = (end-start).normalized()
		diff = n.angle(Vector((0.,1.)))
		if diff > np.pi/4. and diff < np.pi*3./4.:
			m = Vector((1.,0.))
		else:
			m = Vector((0.,1.))
			
		d = (start+(end-start)/2.)*Vector((1-m[0], 1-m[1]))
		start = start*m+d
		end = end*m+d
	if preserve_length:
		midpoint = (start+end)/2.
		fac = length/(start-end).length
		
		start = midpoint+(start-midpoint)*fac
		end = midpoint+(end-midpoint)*fac
		
	
	if interpolation_mode in ['NONE', None]:
		n = (end-start).normalized()
		for lg in loop_groups:
			d = lg.uv-start
			lg.uv = start+d.dot(n)*n
		return
		
	if interpolation_mode == 'EQUIDISTANT':
		t = np.linspace(0,1,len(loop_groups))
	if interpolation_mode == 'LENGTH_RATIO':
		distance = [0]
		for i, lg in enumerate(loop_groups[1:]):
			d = lg[0].vert.co-loop_groups[i][0].vert.co
			distance += [d.length+distance[-1]]
		t = np.array(distance)/distance[-1]
	for i, lg in enumerate(loop_groups):
		lg.uv = start+(end-start)*t[i]
	return

def relax_uvs(loops, uv_layer, iterations=10, pinning=True, border_lock=False, lock_axis=None):
	''' Smoothes position of uv coordinates on the selection based on input arguments
	'''
	
	loop_groups = UVLoopCluster(loops, uv_layer).cleanup()

	c_loop_groups = []
	c_neighbors = []
	for lg in loop_groups:
		if not lg.select:
			continue
		if pinning and lg.pin_uv:
			continue
		if border_lock:
			if lg.vert.is_boundary:
				continue
			if lg.is_boundary():
				continue
		neighbors = lg.neighbors
		c_loop_groups.append(lg)
		c_neighbors.append(neighbors)
		
	for i in range(iterations):
		for lg, neighbors in zip(c_loop_groups, c_neighbors):
			sum = Vector((0.,0.))
			for n in neighbors:
				sum += n.uv
			uv_avg = sum/len(neighbors)
			
			if lock_axis == 'X':
				lg.uv += (uv_avg-lg.uv)*Vector((0.,1.))
			elif lock_axis == 'Y':
				lg.uv += (uv_avg-lg.uv)*Vector((1.,0.))
			else:
				lg.uv = uv_avg
	return

class UVT_OT_multi_object_match_uvs(bpy.types.Operator):
	""" Match the selected objects and their active uv layer according to the active object and uv layer
	"""
	bl_idname = "uv_tools.multi_object_match_uvs"
	bl_label = "Match Active UV Layers"
	bl_options = {"REGISTER", "UNDO"}

	active_name: bpy.props.StringProperty(default='UVMap')

	@classmethod
	def poll(cls, context):
		return context.mode == 'EDIT_MESH'

	def draw(self, context):
		me = context.edit_object.data
		layout = self.layout
		layout.prop_search(self, "active_name", me, "uv_layers", text="")
		return

	def invoke(self, context, event):
		me = context.edit_object.data
		self.active_name = me.uv_layers.active.name
		wm = context.window_manager
		return wm.invoke_props_dialog(self)

	def execute(self, context):
		invalid_objects = []

		for ob in context.selected_editable_objects:
			if not ob.type == 'MESH':
				invalid_objects.append(ob)
				continue
			if not ob.data.uv_layers.get(self.active_name):
				invalid_objects.append(ob)
				continue

		if invalid_objects:
			bpy.ops.object.mode_set(mode='OBJECT')
			for ob in invalid_objects:
				ob.select_set(False)
			bpy.ops.object.mode_set(mode='EDIT')
		for ob in context.selected_editable_objects:
			ob.data.uv_layers.active = ob.data.uv_layers.get(self.active_name)
		return {'FINISHED'}

class UVT_OT_relax_uvs(bpy.types.Operator):
	""" Match the selected objects and their active uv layer according to the active object and uv layer
	"""
	bl_idname = "uv_tools.relax_uvs"
	bl_label = "Relax UVs"
	bl_options = {"REGISTER", "UNDO"}

	pinning: bpy.props.BoolProperty(name='Pinning', default=True)
	border_lock: bpy.props.BoolProperty(name='Lock Borders', default=False)
	iterations: bpy.props.IntProperty(name='Iterations', default=10, min=0, soft_max=100)
	lock_axis: bpy.props.EnumProperty(name='Lock Axis', items=[('NONE', 'None', 'Relax both axes'),
																('X', 'X', 'Lock position on X axis'),
																('Y', 'Y', 'Lock position on Y axis')])

	@classmethod
	def poll(cls, context):
		obj = context.active_object
		return obj and obj.type == 'MESH' and obj.mode == 'EDIT'

	def execute(self, context):
		me = context.active_object.data
		bm = bmesh.from_edit_mesh(me)
		uv_layer = bm.loops.layers.uv.verify()
		
		loops = utils.get_selected_uv_loops(bm, uv_layer)
		
		relax_uvs(loops, uv_layer, pinning = self.pinning, iterations = self.iterations, border_lock=self.border_lock, lock_axis=self.lock_axis)
		
		bmesh.update_edit_mesh(me)
		return {'FINISHED'}

class UVT_OT_line_straighten(bpy.types.Operator):
	""" Match the selected objects and their active uv layer according to the active object and uv layer
	"""
	bl_idname = "uv_tools.line_straighten"
	bl_label = "Straighten Line"
	bl_options = {"REGISTER", "UNDO"}

	interpolation_mode: bpy.props.EnumProperty(name='Interpolation Mode', items=[('NONE', 'None', 'Do not interpolate between endpoints'),
																('EQUIDISTANT', 'Equidistant', 'Interpolate between endpoints with qual distant between all points'),
																('LENGTH_RATIO', 'Length Ratio', 'Interpolate between endpoints based on the length of the respective edges')],
												default='LENGTH_RATIO',)
	auto_snap: bpy.props.BoolProperty(name='Auto Snap', default=True)
	preserve_length: bpy.props.BoolProperty(name='Preserve Length', default=False)

	@classmethod
	def poll(cls, context):
		obj = context.active_object
		return obj and obj.type == 'MESH' and obj.mode == 'EDIT'

	def execute(self, context):
		me = context.active_object.data
		bm = bmesh.from_edit_mesh(me)
		uv_layer = bm.loops.layers.uv.verify()

		loops = utils.get_selected_uv_loops(bm, uv_layer)

		straighten_uv_line(loops,  uv_layer, interpolation_mode=self.interpolation_mode, auto_snap=self.auto_snap, preserve_length=self.preserve_length)
		bmesh.update_edit_mesh(me)
		return {'FINISHED'}

def copy_uvs(loops, uv_layer):
	dict = {}
	for loop in loops:
		dict[str(loop.index)] = loop[uv_layer].uv
	return dict

def paste_uvs(loops, uv_layer, dict):
	indices = [int(key) for key in dict.keys()]
	for loop in loops:
		if loop.index not in indices:
			continue
		loop[uv_layer].uv = dict[str(loop.index)]
	return

class UVT_OT_copy_uvs(bpy.types.Operator):
	""" Stores UV coordinates of selected loops into a buffer
	"""
	bl_idname = "uv_tools.uv_copy"
	bl_label = "Copy"
	bl_options = {"REGISTER", "UNDO"}

	@classmethod
	def poll(cls, context):
		obj = context.active_object
		return obj and obj.type == 'MESH' and obj.mode == 'EDIT'

	def execute(self, context):
		me = context.active_object.data
		bm = bmesh.from_edit_mesh(me)
		uv_layer = bm.loops.layers.uv.verify()

		loops = utils.get_selected_uv_loops(bm, uv_layer)
		
		dict = copy_uvs(loops, uv_layer)
		context.scene['UVT_copy_buffer'] = dict
		
		del bm
		return {'FINISHED'}

class UVT_OT_paste_uvs(bpy.types.Operator):
	""" Writes UV coordinates of selected loops from a buffer
	"""
	bl_idname = "uv_tools.uv_paste"
	bl_label = "Paste"
	bl_options = {"REGISTER", "UNDO"}

	@classmethod
	def poll(cls, context):
		obj = context.active_object
		return obj and obj.type == 'MESH' and obj.mode == 'EDIT' and 'UVT_copy_buffer' in context.scene

	def execute(self, context):
		me = context.active_object.data
		bm = bmesh.from_edit_mesh(me)
		uv_layer = bm.loops.layers.uv.verify()

		loops = utils.get_selected_uv_loops(bm, uv_layer)
		
		dict = context.scene['UVT_copy_buffer']
		paste_uvs(loops, uv_layer, dict)
		
		bmesh.update_edit_mesh(me)
		return {'FINISHED'}

def randomize_islands(loops, uv_layer):
	loop_groups = UVLoopCluster(loops, uv_layer).cleanup()
	islands = loop_groups.split_by_islands()
	
	for island in islands:
		v = Vector((np.random.normal(), np.random.normal()))
		for lg in island:
			lg.uv += v*.1
	
	return

class UVT_OT_randomize(bpy.types.Operator):
	"""
	"""
	bl_idname = "uv_tools.randomize"
	bl_label = "Randomize"
	bl_options = {"REGISTER", "UNDO"}
	
	@classmethod
	def poll(cls, context):
		obj = context.active_object
		return obj and obj.type == 'MESH' and obj.mode == 'EDIT'

	def execute(self, context):
		me = context.active_object.data
		bm = bmesh.from_edit_mesh(me)
		uv_layer = bm.loops.layers.uv.verify()
		
		loops = utils.get_selected_uv_loops(bm, uv_layer)
		
		randomize_islands(loops, uv_layer)
		
		bmesh.update_edit_mesh(me)
		return {'FINISHED'}

classes = [
	UVT_OT_multi_object_match_uvs,
	UVT_OT_relax_uvs,
	UVT_OT_line_straighten,
	UVT_OT_copy_uvs,
	UVT_OT_paste_uvs,
	UVT_OT_randomize,
	]


def register():
	for c in classes:
		bpy.utils.register_class(c)

def unregister():
	for c in classes:
		bpy.utils.unregister_class(c)
