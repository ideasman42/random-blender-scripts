import sys

class UVLoopGroup:
	''' A UVLoopGroup is an object that contains a list of loops from the same BMVert that share the same UV position
		and can be used as a unit for UV operations.
	'''
	
	loops = []
	uv_layer = None
	_uv = None
	_select = None
	
	@property
	def uv(self):
		self._uv = self.loops[0][self.uv_layer].uv
		return self._uv
	
	@uv.setter
	def uv(self, vector):
		self._uv = vector
		for loop in self.loops:
			loop[self.uv_layer].uv = vector
			
	@property
	def select(self):
		self._select = self.loops[0][self.uv_layer].select
		return self._select
	
	@select.setter
	def select(self, state):
		self._select = state
		for loop in self.loops:
			loop[self.uv_layer].select = state
			
	@property
	def pin_uv(self):
		self._select = self.loops[0][self.uv_layer].pin_uv
		return self._select
	
	@pin_uv.setter
	def pin_uv(self, state):
		self._select = state
		for loop in self.loops:
			loop[self.uv_layer].pin_uv = state
	
	@property
	def vert(self):
		return self.loops[0].vert
	
	@property
	def neighbors(self):
		neighbors = []
		for loop in self.loops:
			l = loop.link_loop_prev
			if not any([l in n for n in neighbors]):
				neighbors.append(UVLoopGroup(l, self.uv_layer))
			l = loop.link_loop_next
			if not any([l in n for n in neighbors]):
				neighbors.append(UVLoopGroup(l, self.uv_layer))
		return UVLoopCluster(neighbors, self.uv_layer)
	
	def __init__(self, loop, uv_layer):
		self.uv_layer = uv_layer
		loops = []
		for l in loop.vert.link_loops:
			if l[uv_layer].uv == loop[uv_layer].uv and l not in loops:
				loops.append(l)
		self.loops = loops
	
	def __eq__(self, other):
		for loop in self.loops:
			if loop not in other.loops:
				return False
		for loop in other.loops:
			if loop not in self.loops:
				return False
		return True
	
	def __contains__(self, item):
		return item in self.loops
		
	def __getitem__(self, key):
		return self.loops[key]
	
	def __str__(self):
		return f'<UVLoopGroup({len(self.loops)}): {str(self.uv)[1:-1]}, {self.uv_layer.name}>'
	
	def __len__(self):
		return len(self.loops)
	
	def __bool__(self):
		return bool(self.loops)
	
	def __add__(self, other):
		#TODO: add loop
		if not other.uv_layer == self.uv_layer:
			raise Exception('UV layers not compatible')
		if isinstance(other, UVLoopGroup):
			return UVLoopCluster([self, other], self.uv_layer)
		elif isinstance(other, UVLoopCluster):
			other.loop_groups = [self]+other.loop_groups
			return other
		else:
			return None
	
	def __sub__(self, other):
		return self.pop(other)
	
	def pop(self, loop):
		loops = self.loops.copy()
		for i, l in enumerate(loops):
			if l==loop:
				self.loops.pop(i)
				return self
	
	def loop_index(self, loop):
		for i, l in enumerate(self.loops):
			if loop == l:
				return i
		return False
	
	def is_boundary(self):
		vert = self.vert
		for l in vert.link_loops:
			if l not in self:
				return True
		return False
	
	def remove_hidden(self):
		loops = self.loops.copy()
		for l in loops:
			if l.face.select and not l.face.hide:
				continue
			self.pop(l)
		return self
	
	def remove_unselected(self):
		loops = self.loops.copy()
		for l in loops:
			if l[self.uv_layer].select:
				continue
			self.pop(l)
		return self

class UVLoopCluster:
	''' A UVCluster is an object that contains a list of UVLoopGroups
	'''
	
	loop_groups = []
	uv_layer = None
	
	def __init__(self, items, uv_layer):
		self.uv_layer = uv_layer
		self.loop_groups = []
		for item in items:
			if item in self:
				continue
			if isinstance(item, UVLoopGroup):
				self.loop_groups.append(item)
			else:
				lg = UVLoopGroup(item, uv_layer)
				self.loop_groups.append(lg)
		
	
	def __contains__(self, item):
		if isinstance(item, UVLoopGroup):
			return item in self.loop_groups
		else:
			for lg in self.loop_groups:
				if item in lg:
					return True
		return False
		
	def __getitem__(self, key):
		return self.loop_groups[key]
	
	def __str__(self):
		return f'<UVCluster({len(self.loop_groups)}): {self.uv_layer.name}>'
	
	def __len__(self):
		return len(self.loop_groups)
	
	def __bool__(self):
		return bool(self.loop_groups)
	
	def __add__(self, other):
		if not other.uv_layer == self.uv_layer:
			raise Exception('UV layers not compatible')
		if isinstance(other, UVLoopGroup):
			if other in self:
				return self
			self.loop_groups.append(other)
		elif isinstance(other, UVLoopCluster):
			for lg in other.loop_groups:
				if lg in self:
					continue
				self.loop_groups.append(lg)
		else:
			raise Exception('Object type not compatible for operation')
		return self
	
	def __sub__(self, other):
		if not other.uv_layer == self.uv_layer:
			raise Exception('UV layers not compatible')
		loop_groups = []
		if isinstance(other, UVLoopCluster):
			for lg in self.loop_groups:
				if lg in other:
					continue
				loop_groups.append(lg)
		elif isinstance(other, UVLoopGroup):
			for lg in self.loop_groups:
				if lg is other:
					continue
				loop_groups.append(lg)
		self.loop_groups = loop_groups
		return self
	
	def auto_remove(self):
		remove_indices = []
		for i, lg in enumerate(self.loop_groups):
			if not len(lg.loops):
				remove_indices.append(i)
		
		for i in sorted(remove_indices, reverse=True):
			del self.loop_groups[i]
		return self
	
	def cleanup(self, visible_only=True, selected_only=True):
		for lg in self.loop_groups:
			if visible_only:
				lg.remove_hidden()
			if selected_only:
				lg.remove_unselected()
		
		self.auto_remove()
		return self
	
	def _recursive_traverse(self, root, left):
		neighbors = root.neighbors.cleanup()
		neighbors = UVLoopCluster([n for n in neighbors if n in left], self.uv_layer)
		left -= root
		left -= neighbors
		tail = root
		for n in neighbors:
			tail += self._recursive_traverse(n, left)
		return tail
	
	def ordered(self, first=None):
		if not self.loop_groups:
			return None
		if not first:
			first = self.loop_groups[0]

		rec_lim = sys.getrecursionlimit()
		sys.setrecursionlimit(10000)
		ordered = self._recursive_traverse(first, self)
		left = self-ordered
		
		while len(left)>0:
			first = left[0]
			ordered += self._recursive_traverse(first, left)
			left -= ordered
		
		sys.setrecursionlimit(rec_lim)
		return ordered
	
	def split_by_islands(self):#TODO
		islands = []
		
		left = self
		
		while len(left)>0:
			first = left[0]
			island = self._recursive_traverse(first, left)
			if isinstance(island, UVLoopGroup):
				island = UVLoopCluster(island, self.uv_layer)
			islands.append(island)
			left -= island
			
		return islands
	
	def split_by_direction(self):#TODO
		return
