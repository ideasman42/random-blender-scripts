import bpy
from . import ui, ops

bl_info = {
	"name": "UV Tools",
	"author": "Simon Thommes",
	"version": (0,1),
	"blender": (3, 0, 0),
	"location": "UV Editor > Sidebar > UV Tools",
	"description": "Toolset to perform UV Editing actions.",
	"category": "UV",
}

modules = [ops, ui]

def register():
	for m in modules:
		m.register()

def unregister():
	for m in modules:
		m.unregister()
