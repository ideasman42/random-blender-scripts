import bpy
import numpy as np
from mathutils import Vector

def get_loop_group(loop, uv_layer, border_lock=False):
	''' Returns list of loops at the same position as reference loop, that are selected
	'''
	loop_group = []
	for l in loop.vert.link_loops:
		if l[uv_layer].select and l[uv_layer].uv == loop[uv_layer].uv and l not in loop_group:
			loop_group.append(l)
		elif border_lock:
			return None
	return loop_group

def get_loop_group_index(loop_groups, loop):
	''' Returns index of first loop_group that contains given loop
	'''
	for i, loop_group in enumerate(loop_groups):
		if loop in loop_group:
			return i
	return None

def get_selected_uv_loops(bm, uv_layer):
	''' Returns selected mesh loops of selected UVs
	'''
	loops = []
	for face in bm.faces:
		for loop in face.loops:
			if loop[uv_layer].select:
				loops += [loop]
	return loops

def get_selected_uv_verts(bm, uv_layers):
	"""Returns selected mesh vertices of selected UVs"""
	verts = set()
	for face in bm.faces:
		if face.select:
			for loop in face.loops:
				if loop[uv_layers].select:
					verts.add( loop.vert )
	return list(verts)

def get_selected_uv_edges(bm, uv_layers):
	"""Returns selected mesh edges of selected UVs"""
	verts = get_selected_uv_verts(bm, uv_layers)
	edges = []
	for e in bm.edges:
		if e.verts[0] in verts and e.verts[1] in verts:
			edges += [e]
	return edges

def get_selected_uv_faces(bm, uv_layers):
	"""Returns selected mesh faces of selected UVs"""
	faces = []
	for f in bm.faces:
		if f.select:
			count = 0
			for loop in f.loops:
				if loop[uv_layers].select:
					count+=1
			if count == len(f.loops):
				faces += [f]
	return faces

def get_selection_islands(bm=None, uv_layers=None):
	if bm == None:
		bm = bmesh.from_edit_mesh(bpy.context.active_object.data)
		uv_layers = bm.loops.layers.uv.verify()

	#Reference A: https://github.com/nutti/Magic-UV/issues/41
	#Reference B: https://github.com/c30ra/uv-align-distribute/blob/v2.2/make_island.py

	#Extend selection
	if bpy.context.scene.tool_settings.use_uv_select_sync == False:
		bpy.ops.uv.select_linked()

	#Collect selected UV faces
	faces_selected = []
	for face in bm.faces:
		if face.select and face.loops[0][uv_layers].select:
			faces_selected.append(face)

	#Collect UV islands
	# faces_parsed = []
	faces_unparsed = faces_selected.copy()
	islands = []

	for face in faces_selected:
		if face in faces_unparsed:

			#Select single face
			bpy.ops.uv.select_all(action='DESELECT')
			face.loops[0][uv_layers].select = True
			bpy.ops.uv.select_linked()	#Extend selection

			#Collect faces
			islandFaces = [face]
			for faceAll in faces_unparsed:
				if faceAll != face and faceAll.select and faceAll.loops[0][uv_layers].select:
					islandFaces.append(faceAll)

			for faceAll in islandFaces:
				faces_unparsed.remove(faceAll)

			#Assign Faces to island
			islands.append(islandFaces)

	#Restore selection
	for face in faces_selected:
		for loop in face.loops:
			loop[uv_layers].select = True

	# print("Islands: {}x".format(len(islands)))

	return islands

def is_in_island(island, item):
	if type(item) == bmesh.types.BMEdge:
		for f in island:
			if item in f.edges:
				return True
	elif type(item) == bmesh.type.BMLoop:
		for l in island:
			if item in f.loops:
				return True
	elif type(item) == bmesh.type.BMVert:
		for l in island:
			if item in f.verts:
				return True
	return False
