# Addons

## Border Buddy
A simple addon that adds the functionality to store and manage render borders for easy switching.

## Grease to Mesh
An addon to convert grease pencil animations into mesh sequences.

## Lighting Overrider
An addon for lighting pipeline of the Blender Studio to add and manage override settings to lighting files, store them in json format and make them executable on the render farm.
