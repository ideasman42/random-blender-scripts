Settings:
- channel locking (for everything)

Features:
- Basics:
  - Image rotation
  - Image Flipping
  - Image Resizing (existing)
  - RGB Mapping

- Filters:
  - Custom kernel with UI
  - presets

- Copy Buffer

Display Options with overlays:
- Inspect options:
  - Luminance Display
  - Channel Display


Painting Extension:
- Channel Painting