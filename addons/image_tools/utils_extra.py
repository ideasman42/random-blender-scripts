import bpy
import klembord
from PIL import ImageGrab, Image
import numpy as np
import io

from . import utils

def image_to_clipboard(image):
	img_byte_arr = io.BytesIO()

	img_arr = np.flip(utils.img_to_arr(image), axis=0)
	image = Image.fromarray(np.uint8(img_arr*255))
	image.save(img_byte_arr, format='PNG')
	img_byte_arr.seek(0)

	klembord.init()
	klembord.set({'image/png': img_byte_arr.read()})
	return

def screen_print_to_image(name='buffer', bbox=None):
	copy_buffer = ImageGrab.grab()
	if bbox:
		y_max = copy_buffer.size[1]
		buffer_bbox = (bbox[0], y_max-bbox[3], bbox[2], y_max-bbox[1])
		copy_buffer = copy_buffer.crop(buffer_bbox)

	image = bpy.data.images.new(name, *copy_buffer.size)
	img_arr = np.array(copy_buffer, dtype=np.float32)/255
	img_arr = np.dstack((img_arr, np.full(copy_buffer.size[::-1], 1, dtype=np.float32)))
	img_arr = np.flip(img_arr, axis=0)
	pixels = img_arr.flatten()
	image.pixels.foreach_set(pixels)
	return
