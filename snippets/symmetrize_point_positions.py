import bpy
import mathutils

obj = bpy.context.object

mesh = obj.data
size = len(mesh.vertices)
kd = mathutils.kdtree.KDTree(size)

for i, v in enumerate(mesh.vertices):
    kd.insert(v.co, i)

kd.balance()

for i, v in enumerate(mesh.vertices):
    co = v.co
    if co[0]<0:
        co_mirr, index, dist = kd.find(co.reflect((1,0,0)))
        if index == i:
            v.co[0] = 0
            continue
        v.co = co_mirr.reflect((1,0,0))