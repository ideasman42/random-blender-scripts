import bpy

context = bpy.context

active = context.active_object
selected = context.selected_objects[:]
selected.remove(active)

props = dict(active['_RNA_UI']).keys()

for ob in selected:
    for prop in props:
        if not prop in dict(ob['_RNA_UI']).keys():
            continue
        d = ob.driver_add(f'["{prop}"]').driver
        v = d.variables.new()
        v.name = prop
        v.type = 'SINGLE_PROP'
        v.targets[0].id = active
        v.targets[0].data_path = f'["{prop}"]'
        d.expression = prop