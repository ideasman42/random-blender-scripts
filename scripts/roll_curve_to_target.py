"""
Rolls a Curve with square profile to match a target mesh as best as possible.
Moderately optimized with KD trees and multiprocessing.
"""

import bpy
import mathutils
import time
import numpy as np
import multiprocessing as mp

skin = bpy.data.objects.get("skin")
curve = bpy.data.objects.get("curve")

skin = bpy.data.objects.get("GEO-spiderweb_main.skin")
curve = bpy.data.objects.get("GEO-spiderweb_main.curve")

depsgraph = bpy.context.evaluated_depsgraph_get()

def get_mesh_difference(source, target, origin, number, depsgraph, distance = 1):
    '''Calculates the sum pf distances between a number of points of a mesh around an origin and a target mesh.'''
    
    mesh = source.evaluated_get(depsgraph).to_mesh()
    size = len(mesh.vertices)
    kd = mathutils.kdtree.KDTree(size)

    for i, v in enumerate(mesh.vertices):
        kd.insert(v.co, i)

    kd.balance()
    
    points = np.array(kd.find_n(origin, number), dtype=object).T[0]
    
    sum = 0.
    for v in points:
        result, co, n, i = target.evaluated_get(depsgraph).closest_point_on_mesh(v, distance=1, depsgraph=depsgraph)
        if result:
            sum += (co-v).length
    source.evaluated_get(depsgraph).to_mesh_clear()
    return sum

def process(idx_spline):
    spline = curve.data.splines[idx_spline]
    print(f'Spline {idx_spline+1} out of {len(curve.data.splines)}')
    
    step_size = .2
    for idx_point, point in enumerate(spline.points):
        point.tilt = 0
        l = []
        for i in range(int(np.pi/2./step_size)):
            depsgraph = bpy.context.evaluated_depsgraph_get()
            l += [get_mesh_difference(curve, skin, point.co[:3], 10, depsgraph)]
            point.tilt += step_size
        diff = np.array(l)
        idx = np.argmin(diff)
        if idx ==0:
            up = False
        elif idx == len(diff)-1:
            up = True
        else:
            up = diff[idx-1] <= diff[idx+1]
        p = idx-(diff[idx])/(diff[idx]+diff[idx-1]) if up else idx+(diff[idx])/(diff[idx]+diff[idx+1])
        point.tilt = p*step_size
    return [point.tilt for point in spline.points]


if __name__ == '__main__':
    start = time.time()
    pool = mp.Pool(processes=mp.cpu_count())
    tilts = pool.map(process, range(len(curve.data.splines)))
    
    
    for idx_spline, spline in enumerate(curve.data.splines):
        for idx_point, point in enumerate(spline.points):
            point.tilt = tilts[idx_spline][idx_point]
    
    print(time.time()-start,'s')
