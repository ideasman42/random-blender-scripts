import bpy
import re

from rna_prop_ui import rna_idprop_ui_create


def struct_from_rna_path(rna_path):
    ''' Returns struct object for specified rna path.
    '''
    if not '.' in rna_path:
        return None
    elements = rna_path.rsplit('.', 1)
    string = f"{elements[0]}.bl_rna.properties['{elements[1]}']"
    if not eval(string):
        return None
    return eval(string)

def stylize_name(path):
    ''' Splits words by '_', capitalizes them and separates them with ' '.
    '''
    parent, main = path.rsplit('.', 1)
    try:
        if main in ['default_value']:
            return eval(parent).name
        else:
            return f"{eval(parent).name}: {' '.join([word.capitalize() for word in main.split('_')])}"
    except:
        return ' '.join([word.capitalize() for word in main.split('_')])
    

class OR_OT_addOverrideButton(bpy.types.Operator):
    """Adds an operator on button mouse hover"""
    bl_idname = "overrider.add_override_button"
    bl_label = "Add RNA Override"
    bl_options = {'REGISTER', 'UNDO'}

    rna_path: bpy.props.StringProperty(name="Data path to override", default="")
    override_float: bpy.props.FloatProperty(name="Override", default=0)
    
    init_val = None
    property = None
    override = None

    _array_path_re = re.compile(r'^(.*)\[[0-9]+\]$')

    @classmethod
    def poll(cls, context):
        return bpy.ops.ui.copy_data_path_button.poll()
    
    def draw(self, context):
        layout = self.layout
        property = self.property
        
        if not property:
            return
        
        col = layout.column()
        col.scale_y = 1.8
        col.scale_x = 1.5
        if property.subtype == 'COLOR':
            col.template_color_picker(context.scene, 'override', value_slider=True)
        
        col = layout.column()
        col.prop(context.scene, 'override')
        self.override = context.scene['override']
        
        return

    def invoke(self, context, event):
        clip = context.window_manager.clipboard
        bpy.ops.ui.copy_data_path_button(full_path=True)
        rna_path = context.window_manager.clipboard
        context.window_manager.clipboard = clip
        
        if rna_path.endswith('name'):
            print("Warning: Don't override datablock names.")
            return {'CANCELLED'}

        # Strip off array indices (f.e. 'a.b.location[0]' -> 'a.b.location')
        m = self._array_path_re.match(rna_path)
        if m:
            rna_path = m.group(1)
        
        self.rna_path = rna_path

        self.property = struct_from_rna_path(rna_path)
        
        keys = ['description', 'default', 'min', 'max', 'soft_min', 'soft_max', 'step', 'precision', 'subtype']
        
        vars = {}
        for key in keys:
            try:
                vars[key] = eval(f'self.property.{key}')
            except:
                print(f'{key} not in property')
        
        name = stylize_name(self.rna_path)
        
        if self.property.type == 'FLOAT':
            vars['unit'] = self.property.unit
            if not self.property.is_array:
                bpy.types.Scene.override = bpy.props.FloatProperty(name = name, **vars)
            else:
                vars['size'] = self.property.array_length
                vars['default'] = self.property.default_array[:]
                print(vars['default'])
                bpy.types.Scene.override = bpy.props.FloatVectorProperty(name = name, **vars)
        elif self.property.type == 'STRING':
            bpy.types.Scene.override = bpy.props.StringProperty(name = name, **vars)
        elif self.property.type == 'BOOLEAN':
            bpy.types.Scene.override = bpy.props.BoolProperty(name = name, **vars)
        elif self.property.type == 'INT':
            bpy.types.Scene.override = bpy.props.IntProperty(name = name, **vars)
        elif self.property.type == 'ENUM':
            print('ITEMS',*self.property.enum_items)
            items = [(item.identifier, item.name, item.description, item.icon, i) for i, item in enumerate(self.property.enum_items)]
            vars.pop('subtype', None)
            bpy.types.Scene.override = bpy.props.EnumProperty(items = items, name = name, **vars)

        
        context.scene.override = eval(rna_path)
        
        self.override = context.scene.override
        
        self.init_val = eval(rna_path)

        wm = context.window_manager
        state = wm.invoke_props_dialog(self)
        
        print(state)
        
        if state in {'FINISHED', 'CANCELLED'}:
            del context.scene['override']
            return state
        else:
            return state

    def execute(self, context):
        #override_key = override_keys.generate(self.rna_path)

        #if override_key in context.scene:
        #    self.report({'ERROR'}, "This Override already exists")
        
        if not context.scene.override==self.init_val:
            exec(self.rna_path+f' = context.scene.override')
            del context.scene['override']
            return {'FINISHED'}
        
        del context.scene['override']

        return {'CANCELLED'}
    
    def cancel(self, context):
        del context.scene['override']
        return
        


classes = [
    OR_OT_addOverrideButton,
    ]


def register():
    for c in classes:
        bpy.utils.register_class(c)
    
    wm = bpy.context.window_manager
    if wm.keyconfigs.addon is not None:
        km = wm.keyconfigs.addon.keymaps.new(name="User Interface")
        kmi = km.keymap_items.new("overrider.add_override_button","O", "PRESS",shift=False, ctrl=False)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
