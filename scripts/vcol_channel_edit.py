import bpy

def store_vcol_in_channel(context, source, target, channel):
    """Stores grayscale vertex colors from a source layer on the active object in the specified RGB channel of a target layer
    """
    for loop in context.object.data.loops:
        target.data[loop.index].color[channel] = sum(source.data[loop.index].color[:3])/3.
    return

def read_vcol_from_channel(context, source, target, channel):
    """Reads the specified RGB channel of a source layer on the active object to grayscale colors in a target layer
    """
    for loop in context.object.data.loops:
        target.data[loop.index].color = [source.data[loop.index].color[channel]]*3+[1.]
    return

class VColChannelProps(bpy.types.PropertyGroup):
    vcol_layer: bpy.props.StringProperty(name='Vertex Color Layer')
    editing: bpy.props.BoolProperty(name='Editing', default=False)
    channel: bpy.props.EnumProperty(items=[
                                    ('R', 'Red', 'Red Channel', 0),
                                    ('G', 'Green', 'Green Channel', 1),
                                    ('B', 'Blue', 'Blue Channel', 2)],
                                    name='Channel')

class VColChannelEdit(bpy.types.Operator):
    """Writes the selected channel and vertex color layer to the 'EDIT' vertex colors layer"""
    bl_idname = "mesh.vcol_channel_edit"
    bl_label = "Edit Channel"

    @classmethod
    def poll(cls, context):
        if context.active_object is None:
            return False
        return bool(context.active_object.data.vertex_colors)

    def execute(self, context):
        mesh = context.active_object.data
        settings = mesh.vcol_channel_settings
        
        if not mesh.vertex_colors.get('EDIT'):
            if len(mesh.vertex_colors)>=8:
                return {'CANCELLED'}
            mesh.vertex_colors.new(name='EDIT')
        
        edit = mesh.vertex_colors['EDIT']
        active = mesh.vertex_colors.active
        
        if not active == edit:
            settings.vcol_layer = active.name
        
        layer = mesh.vertex_colors.get(settings.vcol_layer)
        channel_dict = {'R': 0, 'G': 1, 'B': 2,}
        read_vcol_from_channel(context, layer, mesh.vertex_colors['EDIT'], channel_dict[settings.channel])
        mesh.vertex_colors.active = edit
        settings.editing = True
        return {'FINISHED'}
    
class VColChannelStore(bpy.types.Operator):
    """Stores the 'EDIT' vertex colors layer to the selected channel and vertex color layer"""
    bl_idname = "mesh.vcol_channel_store"
    bl_label = "Store Channel"

    @classmethod
    def poll(cls, context):
        if context.active_object is None:
            return False
        return bool(context.active_object.data.vertex_colors)

    def execute(self, context):
        mesh = context.active_object.data
        settings = mesh.vcol_channel_settings
        
        if not mesh.vertex_colors.get('EDIT'):
            return {'CANCELLED'}
        
        layer = mesh.vertex_colors.get(settings.vcol_layer)
        edit = mesh.vertex_colors['EDIT']
        channel_dict = {'R': 0, 'G': 1, 'B': 2,}
        store_vcol_in_channel(context, edit, layer, channel_dict[settings.channel])
        mesh.vertex_colors.active = layer
        settings.editing = False
        return {'FINISHED'}

def draw_vcol_channel(self, context):
    mesh = context.active_object.data
    settings = mesh.vcol_channel_settings
    
    layout = self.layout
    #props = layout.prop(settings, 'vcol_layer')
    if settings.editing:
        layout.prop_search(settings, 'vcol_layer', mesh, "vertex_colors", text="")
    else:
        layout.prop_search(mesh.vertex_colors, "active", mesh, "vertex_colors", text="")
    layout.prop(settings, 'channel', expand=True)
    row = layout.row()
    row.operator('mesh.vcol_channel_edit')
    row.operator('mesh.vcol_channel_store')

classes = [
            VColChannelProps,
            VColChannelStore,
            VColChannelEdit,
            ]

def register():
    for c in classes:
        bpy.utils.register_class(c)
    bpy.types.Mesh.vcol_channel_settings = bpy.props.PointerProperty(type=VColChannelProps)
    bpy.types.DATA_PT_vertex_colors.append(draw_vcol_channel)


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)
    bpy.types.DATA_PT_vertex_colors.remove(draw_vcol_channel)


if __name__ == "__main__":
    register()
