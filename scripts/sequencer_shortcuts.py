import bpy

class TrimStrips(bpy.types.Operator):
    """Tooltip"""
    bl_idname = "sequencer.trim"
    bl_label = "Trim Strips"
    bl_options = {"REGISTER", "UNDO"}

    side: bpy.props.EnumProperty(
        name = "Trim Side",
        description = "Select what side from the cursor is trimmed away",
        items = [
            ('LEFT', "Left", "Trim left side from the cursor"),
            ('RIGHT', "Right", "Trim right side from the cursor"),
        ],
        default='LEFT'
    )

    @classmethod
    def poll(cls, context):
        return bool(context.selected_editable_sequences)

    def execute(self, context):

        bpy.ops.sequencer.split(frame=context.scene.frame_current, type='SOFT', side=self.side)
        bpy.ops.sequencer.delete()
        context.scene.frame_current -= 1
        bpy.ops.sequencer.gap_remove(all=False)
        context.scene.frame_current += 1

        return {'FINISHED'}

class RippleDelete(bpy.types.Operator):
    """ Delete selected strips and remove the created gap"""
    bl_idname = "sequencer.ripple_delete"
    bl_label = "Ripple Delete Strips"
    bl_options = {"REGISTER", "UNDO"}

    @classmethod
    def poll(cls, context):
        return bool(context.selected_editable_sequences)

    def execute(self, context):

        frame = context.scene.frame_current
        frame_selection_start = min([strip.frame_final_start for strip in context.selected_editable_sequences])

        bpy.ops.sequencer.delete()
        context.scene.frame_current = frame_selection_start
        bpy.ops.sequencer.gap_remove(all=False)

        context.scene.frame_current = frame

        return {'FINISHED'}

classes = [
            TrimStrips,
            RippleDelete,]

def register():
    for c in classes:
        bpy.utils.register_class(c)

    wm = bpy.context.window_manager
    if wm.keyconfigs.addon is not None:
        km = wm.keyconfigs.addon.keymaps.new(name="SequencerCommon", space_type="SEQUENCE_EDITOR")
        kmi = km.keymap_items.new("sequencer.ripple_delete","X", "PRESS", ctrl=True)
        kmi = km.keymap_items.new("sequencer.trim","Q", "PRESS", shift=True)
        kmi.properties.side = 'LEFT'
        kmi = km.keymap_items.new("sequencer.trim","W", "PRESS", shift=True)
        kmi.properties.side = 'RIGHT'


def unregister():
    for c in classes:
        bpy.utils.unregister_class(c)


if __name__ == "__main__":
    register()
