import sys
import os
import glob

import subprocess

python_exe = glob.glob(os.path.join(sys.prefix, 'bin', 'python3*'))[0]

subprocess.call([python_exe, "-m", "ensurepip"])
subprocess.call([python_exe, "-m", "pip", "install", "--upgrade", "pip"])
subprocess.call([python_exe, "venv", os.path.join(sys.prefix)])

packages = ['klembord',
            'pillow',
            ]

for p in packages:
    subprocess.call([python_exe, "-m", "pip", "install", p])