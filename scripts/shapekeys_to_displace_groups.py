import bpy
import numpy as np

ob = bpy.data.objects.get("Cube")
mod = ob.modifiers[0]

# collect information

sk_dict = dict()
for sk in ob.data.shape_keys.key_blocks:
    name_split = sk.name.split('.')
    if len(name_split)<2:
        continue
    if name_split[0] not in sk_dict.keys():
        sk_dict[name_split[0]] = [sk]
    else:
        sk_dict[name_split[0]] += [sk]

# build modifier

ng = bpy.data.node_groups.new("Displacement", 'GeometryNodeTree')

loc = np.array([-2000,0])

n_in = ng.nodes.new('NodeGroupInput')
n_in.location = loc
loc += [400,-100]
n_out = ng.nodes.new('NodeGroupOutput')
n_out.location = (200,0)

# create inputs

ng.inputs.new('NodeSocketGeometry', 'Geometry')
ng.outputs.new('NodeSocketGeometry', 'Geometry')

el = ng.inputs.new('NodeSocketVector', 'UVMap')
mod[f'{el.identifier}_use_attribute'] = True

inputs_sk_dict = dict()
inputs_img_dict = dict()
for key, sk_list in sk_dict.items():

    el = ng.inputs.new('NodeSocketImage', f'Image | {key}')
    inputs_img_dict[key] = [el]
    el = ng.inputs.new('NodeSocketFloat', f'Midlevel | {key}')
    inputs_img_dict[key] += [el]
    el.default_value = .5
    el.min_value = 0
    el.max_value = 1

    el = ng.inputs.new('NodeSocketFloat', f'Scale | {key}')
    inputs_img_dict[key] += [el]
    el.default_value = 1
    el.min_value = 0
    el.max_value = 1

    for sk in sk_list:

        el = ng.inputs.new('NodeSocketFloat', f"{'.'.join(sk.name.split('.')[1:])} | {key}")
        inputs_sk_dict[sk] = [el]
        mod[f'{el.identifier}'] = sk.value
        el.min_value = 0
        el.max_value = 1

        el = ng.inputs.new('NodeSocketFloat', f"Mask: {'.'.join(sk.name.split('.')[1:])} | {key}")
        inputs_sk_dict[sk] += [el]
        el.default_value = 1
        el.min_value = 1
        el.max_value = 1
        if not sk.vertex_group:
            continue
        mod[f'{el.identifier}_use_attribute'] = True
        mod[f'{el.identifier}_attribute_name'] = sk.vertex_group

# build node tree


n_setp = ng.nodes.new('GeometryNodeSetPosition')

ng.links.new(n_in.outputs[0], n_setp.inputs[0])
ng.links.new(n_setp.outputs[0], n_out.inputs[0])

n_head_img = None

for key, sk_list in sk_dict.items():
    # influence setup

    n_head = None

    for sk in sk_list:

        loc -= [0,40]
        n = ng.nodes.new('ShaderNodeMath')
        n.operation = 'MULTIPLY'
        ng.links.new(n_in.outputs.get(inputs_sk_dict[sk][0].name), n.inputs[0])
        ng.links.new(n_in.outputs.get(inputs_sk_dict[sk][1].name), n.inputs[1])
        n.hide = True
        n.location = loc
        n_last = n

        if not n_head:
            n_head = n
            continue

        loc += [160,20]
        n = ng.nodes.new('ShaderNodeMath')
        n.operation = 'ADD'
        ng.links.new(n_head.outputs[0], n.inputs[0])
        ng.links.new(n_last.outputs[0], n.inputs[1])
        n.hide = True
        n.location = loc
        loc -= [160,20]
        n_head = n

    # texture setup
    loc += [400,200]
    n_img = ng.nodes.new('GeometryNodeImageTexture')
    ng.links.new(n_in.outputs.get(inputs_img_dict[key][0].name), n_img.inputs[0])
    ng.links.new(n_in.outputs.get('UVMap'), n_img.inputs[1])
    n_img.location = loc
    n_last = n_img


    loc += [300,-100]
    n = ng.nodes.new('ShaderNodeMath')
    n.operation = 'SUBTRACT'
    ng.links.new(n_last.outputs[0], n.inputs[0])
    ng.links.new(n_in.outputs.get(inputs_img_dict[key][1].name), n.inputs[1])
    n.hide = True
    n.location = loc
    n_last = n

    loc -= [0,40]
    n = ng.nodes.new('ShaderNodeMath')
    n.operation = 'MULTIPLY'
    ng.links.new(n_last.outputs[0], n.inputs[0])
    ng.links.new(n_in.outputs.get(inputs_img_dict[key][2].name), n.inputs[1])
    n.hide = True
    n.location = loc
    n_last = n

    loc -= [0,40]
    n = ng.nodes.new('ShaderNodeMath')
    n.operation = 'MULTIPLY'
    ng.links.new(n_last.outputs[0], n.inputs[0])
    ng.links.new(n_head.outputs[0], n.inputs[1])
    n.hide = True
    n.location = loc
    n_last = n

    if not n_head_img:
        n_head_img = n
        loc -= [700,240]
        continue

    loc += [160,60]
    n = ng.nodes.new('ShaderNodeMath')
    n.operation = 'ADD'
    ng.links.new(n_head_img.outputs[0], n.inputs[0])
    ng.links.new(n_last.outputs[0], n.inputs[1])
    n.hide = True
    n.location = loc
    loc -= [160,60]
    n_head_img = n

    loc -= [700,240]

loc = np.array([-200,-100])
n = ng.nodes.new('ShaderNodeVectorMath')
n.operation = 'SCALE'
ng.links.new(n.outputs[0], n_setp.inputs[3])
ng.links.new(n_head_img.outputs[0], n.inputs[3])
n.location = loc
n_last = n

loc += [-200,0]
n = ng.nodes.new('GeometryNodeInputNormal')
ng.links.new(n.outputs[0], n_last.inputs[0])
n.location = loc

list = ['NodeGroupOutput', 'GeometryNodeInputNormal', 'GeometryNodeSetPosition', 'ShaderNodeVectorMath', 'NodeGroupInput', 'ShaderNodeMath', 'GeometryNodeImageTexture']


mod.node_group = ng
