'''
Bakes custom normals into NORMAL vertex color layer on selected objects.
'''
import bpy

def vector_to_vcol(vec):
    '''Returns mapped color between [0,1].'''
    return [(v+1.)/2. for v in vec]+[1.]

def bake_normals_to_vcol(context, ob):
    '''Bakes custom normals into NORMAL vertex color layer on object.'''
    # turn off modifiers
    mod_vis = []
    for mod in ob.modifiers:
        mod_vis += [mod.show_viewport]
        mod.show_viewport = False
    
    # get/create vcol layer
    vcol = ob.data.vertex_colors.get("NORMAL")
    if not vcol:
        vcol = ob.data.vertex_colors.new(name="NORMAL")
    
    # write custom normals to vcol layer
    depsgraph = context.evaluated_depsgraph_get()
    ob.evaluated_get(depsgraph).data.calc_normals_split()
    for loop in ob.evaluated_get(depsgraph).data.loops:
            vcol.data[loop.index].color = vector_to_vcol(loop.normal)
    
    # restore modifier visibility
    for mod, vis in zip(ob.modifiers, mod_vis):
        mod.show_viewport = vis


if __name__ == "__main__":
    context = bpy.context
    for ob in context.selected_editable_objects:
        bake_normals_to_vcol(context, ob)